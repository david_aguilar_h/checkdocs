const puppeteer = require('puppeteer-extra');

const captcha =  require('./helper/captcha')

const StealthPlugin = require('puppeteer-extra-plugin-stealth');

puppeteer.use(StealthPlugin());

class invoice {

    static async check( { folio, rfcemisor, rfcreceptor }){
        // page selectors
        const folioSelector = "#ctl00_MainContent_TxtUUID";
        const rfcEmisorSelector = "#ctl00_MainContent_TxtRfcEmisor";
        const rfcReceptorSelector = "#ctl00_MainContent_TxtRfcReceptor";
        const captchaInputSelector = "#ctl00_MainContent_TxtCaptchaNumbers";
        const successResultSelector = "#ctl00_MainContent_PnlResultados";
        const emptyResutlSelector = "#ctl00_MainContent_PnlNoResultados";
        const errorResutlSelector = "#ctl00_MainContent_VsResumenErrores";
        try {
            console.log(` folio : ${folio} - rfc emisor: ${rfcemisor} - rfc receptor: ${rfcreceptor}`);
            const browser = await puppeteer.launch({ headless: false });
            const page =  await browser.newPage();
            await page.goto("https://verificacfdi.facturaelectronica.sat.gob.mx/"); // go to page
            await page.waitForTimeout(1000);
            
            // fill inputs on page 
            await page.type(folioSelector, folio); // Folio Fiscal
            await page.type(rfcEmisorSelector, rfcemisor); // RFC Emisor
            await page.type(rfcReceptorSelector, rfcreceptor); // RFC Receptor
            
            const urlCaptcha  = await page.evaluate( ()=>{ // get te captcha URL
                const data = document.querySelector("#ctl00_MainContent_ImgCaptcha");
                return data.src;
            });
            const solveCaptcha = await captcha.solve(urlCaptcha);
            await page.type(captchaInputSelector, solveCaptcha); // enter captcha 
            
            await page.click("#ctl00_MainContent_BtnBusqueda"); // click on search

            await page.waitForTimeout(1000); // wait for page load

            let succesResult = false;
            let emptyResult = false;
            let errorResult = false;
            // check if success 
            try {
                await page.waitForSelector(successResultSelector, { timeout: 1000 });
                succesResult = true;
            } catch (error) {
                succesResult = false;
            }
            // check if empty
            try {
                await page.waitForSelector(emptyResutlSelector, { timeout: 1000 });
                emptyResult = true;
            } catch (error) {
                emptyResult = false;
            }
            // checkIFError
            try {
                await page.waitForSelector(errorResutlSelector, { timeout: 1000 });
                errorResult = true;
            } catch (error) {
                errorResult = false;
            }
            let responseData; 

            switch(true){
                case succesResult:
                    responseData = await page.evaluate( () => {
                        const estadoFactura = document.querySelector("#ctl00_MainContent_LblEstado").innerText;
                        const monto = document.querySelector("#ctl00_MainContent_LblMonto").innerText;
                        const fechaExpedicion = document.querySelector("#ctl00_MainContent_LblFechaEmision").innerText;
                        const fechaCertificacion = document.querySelector("#ctl00_MainContent_LblFechaCertificacion").innerText;
                        const pac = document.querySelector("#ctl00_MainContent_LblRfcPac").innerText;
                        return {
                            success: 'true',
                            monto,
                            estadoFactura,
                            fechaExpedicion,
                            fechaCertificacion,
                            pac
                        }
                    })
                    break;
                case emptyResult:
                    responseData = await page.evaluate( ()=> {
                        const mensaje = document.querySelector("#ctl00_MainContent_PnlNoResultados h3").innerText;
                        return {
                            success: 'false',
                            mensaje
                        }
                    }) 
                    break;
                case errorResult:
                    responseData = await page.evaluate( ()=> {
                        const mensaje = document.querySelector("#ctl00_MainContent_VsResumenErrores").innerText;
                        return {
                            success: 'false',
                            mensaje
                        }
                    })
                    break;
                default:
                    responseData = {
                        success: 'false',
                        mensaje: ' Error desconocido '
                    }
            }

            await browser.close();

            return responseData;
            
        } catch (error) {
            return error;
        }
    }
}

module.exports = invoice;

