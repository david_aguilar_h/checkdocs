const { createWorker } = require('tesseract.js');


class ocr {
    
    static async read(image){

        const worker = createWorker({
            cachePath: "./",
            logger: m => console.log(m)
        });

        try {
            await worker.load();
            await worker.loadLanguage('eng');
            await worker.initialize('eng');
            const { data: { text } } = await worker.recognize(image);
            await worker.terminate();
            const res = text.replace(/\W/g, '');
            console.log(res);
            return {res}
        } catch (error) {
            console.log(error)
        }

    }
}

module.exports = ocr;