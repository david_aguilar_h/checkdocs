const Client = require('@infosimples/node_two_captcha');

const client = new Client('b7b0bf45bb9db93fec44a75c7bb270ae', { // api key 2captcha  - 2captcha.com
    timeout: 120000, // timeout for trying get answere
    polling: 6000, // min recomended by 2captcha 5 seg
    throwErrors: false // if throw error or log error 
});

// const attempts = 2;

class captcha {

    // normal captcha - image number captcha
    static async solve(imageURL){

        try {
            console.log("start solving captcha ...");
            const response =  await client.decode({ url: imageURL });
            const solveCaptcha = response._text;
            console.log('resolve captcha, result is: ', solveCaptcha);
            return solveCaptcha;
        } catch (error) {
            console.log("error on solve: ", error)
        }

    }
    // recaptcha v2 - i'm not a robot 
    static async solveV2(key, url){
        let run = 1;
        try {
            console.log("start solving captcha ...", key, url);
            const response = await client.decodeRecaptchaV2({
                googlekey: key,
                pageurl: url
            })
            let solveCaptcha = response?._text;
            // TODO get id from 2 captcha for try again later
            /* if(typeof solveCaptcha === "undefined" && run <= attempts ){
                console.log("retryin captcha solve ...");
                run++;
                solveCaptcha =  await this.solveV2(key, url);
            } */
            console.log('resolve captcha, result is: ', solveCaptcha);
            return solveCaptcha;

        } catch (error) {
            console.log("error on solve v2:",error);
        }
    }

}

module.exports = captcha;