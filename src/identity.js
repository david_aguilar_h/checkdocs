const puppeteer = require('puppeteer-extra');

const captcha =  require('./helper/captcha')

const StealthPlugin = require('puppeteer-extra-plugin-stealth');

puppeteer.use(StealthPlugin());

class identity {

    static async checkByCurp(curp){
        const siteUrl = "https://www.gob.mx/curp";
        try {
            const browser = await puppeteer.launch({ headless: false });
            const page =  await browser.newPage();
            await page.goto(siteUrl); // go to page
            await page.waitForTimeout(1000);
            await page.type("#curpinput", curp ); // input curp

            const keyCaptcha = await page.evaluate( ()=>{
                document.querySelector("#g-recaptcha-response").style.display = "block";
                const url  = document.querySelector("#captcha iframe").src;
                const baseUrl = new URL(url).searchParams;
                const key = baseUrl.get('k');
                return key;
            })
            const resolve  = await captcha.solveV2(keyCaptcha, siteUrl);
            await page.type("#g-recaptcha-response", resolve);
            await page.click("#searchButton");
            
            await page.waitForTimeout(1000);
            
            await page.waitForSelector("#ember335 table tr");

            const response = await page.evaluate( ()=> {
                const rows = document.querySelectorAll("#ember335 table tr");
                const object = [].reduce.call(rows, function(res, row) {
                    res[row.cells[0].textContent.slice(0,-1).trim().replace('\n', '')] = row.cells[1].textContent.trim().replace('\n', '');
                    return res;
                }, {});
                // const res = JSON.stringify(object);
                return object;
            })

            console.log(response);

            await browser.close();

            return response;
            
        } catch (error) {
            console.log(error)
        }
    }

    static async checkByInfo( info ){
        const siteUrl = "https://www.gob.mx/curp";
        try {
            const browser = await puppeteer.launch({ headless: false });
            const page =  await browser.newPage();
            await page.goto(siteUrl); // go to page
            await page.waitForTimeout(1000);

            await page.click("#datos a");

            // input formulario 
            await page.type("#nombre", info.nombres);
            await page.type("#primerApellido", info.apellido1);
            await page.type("#segundoApellido", info.apellido2);
            await page.select("#diaNacimiento", info.dia);
            await page.select("#mesNacimiento", info.mes);
            await page.type("#selectedYear", info.ano);
            await page.select("#sexo", info.sexo);
            await page.select("#claveEntidad", info.estado); 

            const keyCaptcha = await page.evaluate( ()=>{
                document.querySelector("#g-recaptcha-response").style.display = "block";
                const url  = document.querySelector("#captcha iframe").src;
                const baseUrl = new URL(url).searchParams;
                const key = baseUrl.get('k');
                return key;
            })

            // resolve captcha 
            const resolve  = await captcha.solveV2(keyCaptcha, siteUrl);
            await page.type("#g-recaptcha-response", resolve);
            await page.click("#searchButton");
            
            await page.waitForTimeout(1000);
            
            await page.waitForSelector("#ember335 table tr");

            const response = await page.evaluate( ()=> {
                const rows = document.querySelectorAll("#ember335 table tr");
                const object = [].reduce.call(rows, function(res, row) {
                    res[row.cells[0].textContent.slice(0,-1).trim().replace('\n', '')] = row.cells[1].textContent.trim().replace('\n', '');
                    return res;
                }, {});
                // const res = JSON.stringify(object);
                return object;
            })

            console.log(response);

            await browser.close();
            
            return response;
            
        } catch (error) {
            console.log(error)
        }
    }
}

module.exports = identity;