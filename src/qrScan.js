const Jimp = require("jimp");
const fs = require('fs')
const qrCode = require('qrcode-reader');

class qrScan{

    static async decode(){
        console.log("start reading qr from invoice");
        const buffer = fs.readFileSync('./public/img/facturas/factura_1.jpg');

        // Parse the image using Jimp.read() method
        const image = await Jimp.read(buffer);
        const qrcode = new qrCode();
        const value = await new Promise((resolve, reject) => {
            qrcode.callback = (err, v) => err != null ? reject(err) : resolve(v);
            qrcode.decode(image.bitmap);
        });

        let query = new URLSearchParams(value.result);
        console.log("qr params: ", query);
        const result = {
            folio: query.get('id'),
            rfcemisor: query.get('re'),
            rfcreceptor: query.get('rr')
        }
        return result;
    }
}

//qrScan.decode()
module.exports = qrScan;