const express = require("express");
const path = require('path');
const sharp = require('sharp');

const invoice = require('./src/invoice');
const identity = require('./src/identity');
const ocr = require('./src/ocr');
const qrScan = require('./src/qrScan');

const app = express();

app.use(express.json({ limit: "20mb" } )) // for parsing application/json
app.use(express.urlencoded({ extended: true })) // for parsing application/x-www-form-urlencoded
app.use(express.static(path.join(__dirname, 'public')));

app.get('/', (req, res)=>{
    res.send('hello world! :D ');
});

app.post('/invoice', async(req, res)=>{
    const { folio, rfcemisor, rfcreceptor, name } = req.body;
    console.log("start checking invoice name: ", name);
    const data = await invoice.check({ folio, rfcemisor, rfcreceptor });
    console.log(data);
    res.json(data);
});

app.post('/identity-curp', async (req, res) =>{
    const { curp } = req.body;
    console.log("start checking identity by CURP:", curp);
    const data = await identity.checkByCurp(curp);
    res.json(data);
});

app.post('/identity-info', async (req, res) =>{
    const { nombres, apellido1, apellido2, dia, mes, ano, sexo, estado, tipo } = req.body;
    console.log("start checking identity by info of type: ", tipo);
    const data = await identity.checkByInfo({ nombres, apellido1, apellido2, dia, mes, ano, sexo, estado });
    res.json(data);
});

app.post('/ocr', async (req, res) =>{
    const { image } = req.body;
    console.log("start read img by ocr");
    const base64Data = image.replace(/^data:image\/png;base64,/,"");
    const imageBuffer = Buffer.from(base64Data, "base64");
    // UPSCALING
    let imageResult = await sharp(imageBuffer).resize({ height: 500 }).toBuffer();
    const string64 = imageResult.toString("base64");
    const resizedBase64 = `data:image/png;base64,${string64}`;

    const data =await ocr.read(resizedBase64);
    res.json(data);
})

app.post('/qrdecode', async (req, res) => {
    const data = await qrScan.decode();
    console.log("data",data);
    res.json(data);
});

app.post('/read-search', async (req, res) => {

    const data = await qrScan.decode();
    const response = await invoice.check(data);
    console.log(response);
    res.json(response);
});

app.listen(3000, () => {
    console.log("El servidor está inicializado en el puerto 3000");
});