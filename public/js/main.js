
let cropModal;
let image;
const url = "http://localhost:3000";
const config = {
  headers: { 'Content-Type': 'application/json' }
}

window.onload = ()=>{

  image = document.getElementById('image');
  cropModal = new bootstrap.Modal(document.getElementById('cropModal'), {})

}

function showModal() {

  cropModal.show();
  crop.getImage();

}

async function sendFragment(){

  // const form = new FormData();
  const blobImage = crop.getBlob();
  const base64Image = await toBase64(blobImage);
  //form.append("image", base64Image);

  const response = await axios.post(`${url}/ocr`, { image: base64Image }, config);

  crop.setInput(response.data.res);
  cropModal.hide();

}

async function sendData(){

  const folio = document.getElementById('folio').value;
  const rfcemisor = document.getElementById('rfcemisor').value;
  const rfcreceptor = document.getElementById('rfcreceptor').value;
  const res = await axios.post(`${url}/invoice`, { folio, rfcemisor, rfcreceptor}, config )
  document.getElementById('responseJson').innerText = JSON.stringify(res.data, undefined, 2);
}

function toBase64(blob){
  return new Promise((resolve, _) => {
    const reader = new FileReader();
    reader.onloadend = () => resolve(reader.result);
    reader.readAsDataURL(blob);
  });
}