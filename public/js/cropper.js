
class crop{

    constructor(){
        this.cropper;
        this.cropedBlobImage;
        this.selectedInput;
    }

    static newCropper(e, image) {

        this.cropper ? this.destroy() : null;

        this.selectedInput = e;
        this.cropper = new Cropper(image, {
            viewMode: 1,
            autoCropArea: 0.2,
            initialAspectRatio:  16/9,
            crop(event) {
              console.log("coordinates:", `x: ${event.detail.x} - y: ${event.detail.y} - width: ${event.detail.width} - height: ${event.detail.height}`)
            },
        });
    }

    static getImage(){
        const cropedImage = this.cropper.getCroppedCanvas({
            imageSmoothingEnabled: true,
            imageSmoothingQuality: 'high',
        }).toDataURL("image/png");
        this.cropper.getCroppedCanvas().toBlob((blob)=>{
            this.cropedBlobImage = blob;
        });
        document.querySelector("#imageCroped").src = cropedImage;
    }

    static getBlob(){
        return this.cropedBlobImage;
    }

    static setInput(text){
        this.selectedInput.value = text;
    }

    static destroy(){
        this.cropper.destroy();
    }
}